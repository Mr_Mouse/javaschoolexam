package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private String[] operators = {"(", ")", "+", "-", "*", "/"};

    public String evaluate(String statement) {
        // Validate arguments
        if (!statementIsValid(statement)) {
            return null;
        }

        // Separate string with spaces and than spliting it into string array.
        statement = statement.replace(" ", "");
        for (String currentOperator : operators) {
            statement = statement.replace(currentOperator, " " + currentOperator + " ");
        }
        String[] separatedStatement = statement.split(" ");

        // Converting string array with statement to reverse polish notation
        ArrayList<String> rpnList = convertToReversePolishNotation(separatedStatement);

        // Evaluate reverse polish notation
        Stack<Double> evaluationStack = new Stack<>();
        if (!evaluateReversePolishNotation(rpnList, evaluationStack)){
            return null;
        }

        // Extraction of result from stack
        Double result;
        if (!evaluationStack.isEmpty()) {
            result = evaluationStack.pop();
        } else {
            return null;
        }

        // Rounding of result
        result = BigDecimal.valueOf(result).setScale(4, BigDecimal.ROUND_HALF_DOWN).doubleValue();

        // Converting result to string and cutting fraction if it's useless, equals 0
        String strResult = result.toString();
        if (strResult.length() > 2 && (strResult.substring(strResult.length() - 2)).equals(".0")) {
            strResult = strResult.substring(0, (strResult.length() - 2));
        }

        return strResult;
    }

    /**
     * Validate statement, can consist:
     *  - Digits (0-9)
     *  - Dots as decimal marks (valid example: 100.02, not valid example : 100..02)
     *  - Parentheses
     *  - Mathematical symbols (allowed are : "+", "-", "*", "/")
     * Also validate sequence of opening and closing parenthesis.
     *
     * @param statement statement for check
     * @return <code>true</code> if all conditions are met, otherwise <code>false</code>
     */
    private boolean statementIsValid(String statement) {
        if (statement == null) {
            return false;
        }

        String statementWithoutSpaces = statement.replace(" ", "");

        if (statement.equals("")) {
            return false;
        }

        final String OPERATORS_SYMBOLS = ".()+-*/";
        final String NUMBER_SYMBOLS = "0123456789";
        char[] charArray = statementWithoutSpaces.toCharArray();
        int parenthesisStack = 0;
        for (int i = 0; i < charArray.length; i++) {
            if (NUMBER_SYMBOLS.indexOf(charArray[i]) == -1
                    && OPERATORS_SYMBOLS.indexOf(charArray[i]) == -1) {
                return false;
            }

            if ((charArray[i] == '.') && (i + 1 != charArray.length) && (i - 1 >= 0)) {
                if (NUMBER_SYMBOLS.indexOf(charArray[i-1]) == -1
                        || NUMBER_SYMBOLS.indexOf(charArray[i+1]) == -1) {
                    return false;
                }
            }

            if (charArray[i] == '(') {
                parenthesisStack++;
            } else if (charArray[i] == ')') {
                parenthesisStack--;
                if (parenthesisStack < 0) {
                    return false;
                }
            }
        }
        return parenthesisStack == 0;
    }

    /**
     * Converting provided statement to reverse polish notation according wiki article
     * https://ru.wikipedia.org/wiki/%D0%9E%D0%B1%D1%80%D0%B0%D1%82%D0%BD%D0%B0%D1%8F_%D0%BF%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F_%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D1%8C
     *
     * @param separatedStatement string array contains statement
     * @return ArrayList with statement writed in reverse polish notation
     */
    private ArrayList<String> convertToReversePolishNotation(String[] separatedStatement) {
        ArrayList<String> rpnList = new ArrayList<>();
        Stack<String> operatorsStack = new Stack<>();

        for (String element : separatedStatement) {

            // If provided element is number, just write it in ArrayList
            if (isNumber(element)) {
                rpnList.add(element);
            } else if (isOperator(element)) {

                // In operators case need to check stack is empty or not
                // If it's empty provided element not closing parentheses,
                // push element to stack.
                // If element is closing parentheses return null - statement recorded incorrectly.
                if (operatorsStack.isEmpty()) {
                    if (!element.equals(")")) {
                        operatorsStack.push(element);
                    } else {
                        return null;
                    }
                } else {

                    // In case stack isn't empty and element is opening parentheses,
                    // just write it in ArrayList
                    if (element.equals("(")) {
                        operatorsStack.push(element);

                    // If element is closing parentheses,
                    // pop element from the stack consequentially to ArrayList
                    // until opening parentheses, than pop opening parentheses,
                    // but not move it to ArrayList
                    // If opening parentheses are not met return null - statement recorded incorrectly.
                    } else if (element.equals(")")) {
                        while (!operatorsStack.isEmpty() && !operatorsStack.peek().equals("(")){
                            rpnList.add(operatorsStack.pop());
                        }
                        if (operatorsStack.isEmpty()) {
                         return null;
                        } else {
                            operatorsStack.pop();
                        }

                    // If weight of current operator less or equal to
                    // weight of operator on top of stack - move top operator from stack to ArrayList
                    // and push current operator to stack
                    } else if (weightOfOperator(element) <= weightOfOperator(operatorsStack.peek())) {
                        rpnList.add(operatorsStack.pop());
                        operatorsStack.push(element);
                    // Otherwise push element to stack
                    } else {
                        operatorsStack.push(element);
                    }
                }
            }
        }

        // Pop operators from the stack consequentially to ArrayList
        while (!operatorsStack.isEmpty()){
            rpnList.add(operatorsStack.pop());
        }
        return rpnList;
    }

    /**
     * Method evaluate statement in reverse polish notation.
     * According to algorithm described in wiki article
     * https://ru.wikipedia.org/wiki/%D0%9E%D0%B1%D1%80%D0%B0%D1%82%D0%BD%D0%B0%D1%8F_%D0%BF%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F_%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D1%8C
     * Moving elements at the beginning of ArrayList, if it's numbers just move it to stack.
     * If it's operator, pop two elements from stack and perform calculation with it.
     *
     * @param rpnList statement in reverse polish notation
     * @param evaluationStack stack for performing evaluation reverse polish notation way
     * @return <code>true</code> if all calculation done right, otherwise <code>false</code>
     */
    private boolean evaluateReversePolishNotation(ArrayList<String> rpnList,
                                                  Stack<Double> evaluationStack) {
        if (rpnList != null) {
            for (String element : rpnList) {

                if (isNumber(element)) {
                    evaluationStack.add(Double.parseDouble(element));
                } else {
                    double b = evaluationStack.pop();
                    double a = evaluationStack.pop();

                    switch (element) {
                        case "+":
                            evaluationStack.add(a + b);
                            break;
                        case "-":
                            evaluationStack.add(a - b);
                            break;
                        case "*":
                            evaluationStack.add(a * b);
                            break;
                        case "/":
                            if (b != 0) {   // Check's zero division
                                evaluationStack.add(a / b);
                            } else {
                                return false;
                            }
                            break;
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return weight of operator according to reverse polish notation.
     *
     * @param element operator to check
     * @return int with weight of operator
     */
    private int weightOfOperator(String element) {
        switch (element) {
            case "(":
            case ")":
                return 0;
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            default:
                return -1;
        }
    }

    /**
     * Method check provided element operator or not
     * using string array with operators.
     *
     * @param element string element to check
     * @return <code>true</code> if provided element is arithmetic operator, otherwise <code>false</code>
     */
    private boolean isOperator(String element) {
        for (String currentOperator : operators) {
            if (element.equals(currentOperator)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method check provided string is number or not
     * using conversion from string to double.
     *
     * @param number string supposedly with number
     * @return <code>true</code> if provided string contains Double number, otherwise <code>false</code>
     */
    private boolean isNumber(String number) {
        try {
            Double.parseDouble(number);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
