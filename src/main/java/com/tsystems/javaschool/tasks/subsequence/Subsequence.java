package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // Validate arguments
        if (x == null || y == null) {
            throw new java.lang.IllegalArgumentException();
        } else if (x.isEmpty() && y.isEmpty()) {
            return true;
        } else if (y.size() > 0 && x.isEmpty()) {
            return true;
        } else if (x.size() > 0 && y.isEmpty()) {
            return false;
        }

        // Going through second sequence "y" and searching for
        // symbol in first sequence "x" with index "j" (from first to last symbol).
        // If symbol from first sequence found, increase "j" index,
        // and keep searching in second sequence.
        // If j index + 1 has reached size of first sequence
        // or in the j + 1 equals size of "y" sequence
        // it means sequence "y" contains sequence "x".
        int j = 0;
        for (Object aY : y) {
            if (j + 1 == x.size()) {
                return true;
            }
            if (x.get(j).equals(aY)) {
                    j++;
            }
        }
        return j + 1 == y.size();
    }
}
