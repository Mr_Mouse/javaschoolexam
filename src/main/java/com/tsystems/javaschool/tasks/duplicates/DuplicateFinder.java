package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        // Validate arguments
        if (sourceFile == null || targetFile == null) {
            throw new IllegalArgumentException();
        }

        // Checking source file exist or not. If exist read data from it,
        // else exit from program and return false
        boolean resultOfProcess = false;
        if (sourceFile.exists()) {
            TreeMap<String, Integer> uniqueLines = getUniqueLinesFromFile(sourceFile);

            // Checking output file exist or not. If doesn't exist
            // create file, otherwise just use existing file.
            // Then append data in provided file.
            if (targetFile.exists()) {
                writeToFile(targetFile, uniqueLines);
                resultOfProcess = true;
            } else {
                try {
                    if (targetFile.createNewFile()) {
                        writeToFile(targetFile, uniqueLines);
                        resultOfProcess = true;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return resultOfProcess;
    }

    /**
     * Method append data from TreeMap to provided text file.
     *
     * @param targetFile file were to write data
     * @param uniqueLines data structure with string lines and quantity of lines
     */
    private void writeToFile(File targetFile, TreeMap<String, Integer> uniqueLines) {
        try {
            FileWriter writer = new FileWriter(targetFile.getAbsolutePath(), true);

            try (BufferedWriter bufferWriter = new BufferedWriter(writer)) {
                for (Map.Entry<String, Integer> entry : uniqueLines.entrySet()) {
                    bufferWriter.write(entry.getKey() + "[" + entry.getValue() + "]\n");
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method read data form source file. Than folds list of strings,
     * before that counting number of duplicates.
     * Write list of unique strings and number of duplicates into TreeMap for auto sort purpose.
     *
     * @param sourceFile file for reading
     * @return data structure with string lines and number of repetitions in provided source file
     */
    private TreeMap<String, Integer> getUniqueLinesFromFile(File sourceFile) {
        TreeMap<String, Integer> uniqueLines = new TreeMap<>();

        try {
            try (BufferedReader in = new BufferedReader(new FileReader(sourceFile.getAbsoluteFile()))) {
                String stringFromFile;
                int numberOfDuplicates;
                while ((stringFromFile = in.readLine()) != null) {
                    if (uniqueLines.containsKey(stringFromFile)) {
                        numberOfDuplicates = uniqueLines.get(stringFromFile);
                        uniqueLines.put(stringFromFile, ++numberOfDuplicates);
                    } else {
                        uniqueLines.put(stringFromFile, 1);
                    }
                }
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
        return uniqueLines;
    }
}
